package com.backend.controles;

import com.backend.entidades.Calidadproducto;
import com.backend.repositorios.repocalidadproducto;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author jhoan
 */
@RestController
@Service
@EnableJpaRepositories(basePackages = "com.backend.repositorios")
public class ControlCalidadPro {

    @Autowired
    repocalidadproducto rcp;

    @CrossOrigin
    @RequestMapping(
            value = "calidadpro/all",
            method = RequestMethod.GET,
            produces = "application/json"
    )
    public List<Calidadproducto> getall() {
        List<Calidadproducto> result = (List<Calidadproducto>)rcp.findAll();
        return result;
    }
    
    @RequestMapping(
            value = "calidadpro/crear",
            method = RequestMethod.POST,
            produces = "application/json"
    )
    public Calidadproducto create(@RequestBody Calidadproducto cali){
        cali = rcp.save(cali);
        return cali;
    }
    @RequestMapping (
            value ="calidadpro/actualizar",
            method = RequestMethod.PUT,
            consumes = "application/json"
    )
    public Calidadproducto actualizar(@RequestBody Calidadproducto pro){
        pro = rcp.save(pro);
        return pro;
    }
    
    @RequestMapping(
            value = "calidapro/eliminar",
            method = RequestMethod.POST
    )
        public String eliminar(@RequestBody Calidadproducto idcalidadproducto){
            try {
            //System.out.println("Estoy entrando al try");    
            rcp.delete(idcalidadproducto);
            return "Exito Eliminando"+idcalidadproducto.getCalidadproducto();
        } catch (Exception e) {
            
            return "Error al Eliminar"+e.toString();
        }
        //return "Exito";
    }
}
