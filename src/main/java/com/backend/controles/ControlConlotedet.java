package com.backend.controles;

import com.backend.entidades.Controllotedet;
import com.backend.repositorios.repocontrollotedet;
import java.util.List;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author jhoan
 */
@RestController
@Service
@EnableJpaRepositories("com.backend.repositorios")
public class ControlConlotedet {
    
    @Autowired
    repocontrollotedet rcld;
    
    @CrossOrigin
    @RequestMapping(
            value = "controllotedet/all",
            method = RequestMethod.GET,
            produces = "application/json"
    )
    public List<Controllotedet> getall() {
        List<Controllotedet> result = (List<Controllotedet>) rcld.findAll();
        return result;
    }
    
    @RequestMapping(
            value = "controllotedet/crear",
            method = RequestMethod.POST,
            produces = "application/json"
    )
    public Object crear(@RequestBody Controllotedet detlote) {
        JSONObject js = new JSONObject();
        try {
            detlote = rcld.save(detlote);
            js.append("Exito", 1);
            return js.get("Exito");
        } catch (Exception e) {
            js.append("Error", 0);
            return js.get("Error");
        }
    }
    
    @RequestMapping(
            value = "controllotedet/actualizar",
            method = RequestMethod.PUT, produces = "application/json"
    )
    public Object actualizar(@RequestBody Controllotedet detlote) {
        JSONObject js = new JSONObject();
        try {
            detlote = rcld.save(detlote);
            js.append("Exito", 1);
            return js.get("Exito");
        } catch (Exception e) {
            js.append("Error", 0);
            
            return js.getJSONObject("Error");
            
        }
    }
}
