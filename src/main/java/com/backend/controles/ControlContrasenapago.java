package com.backend.controles;

import com.backend.entidades.Contrasenapago;
import com.backend.repositorios.repocontrasenapago;
import java.util.List;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author jhoan
 */
@RestController
@Service
@EnableJpaRepositories("com.backend.repositorios")
public class ControlContrasenapago {

    @Autowired
    repocontrasenapago rcp;

    @CrossOrigin
    @RequestMapping(
            value = "contrasena/all",
            method = RequestMethod.GET,
            produces = "application/json"
    )
    public List<Contrasenapago> getall() {
        List<Contrasenapago> result = (List<Contrasenapago>) rcp.findAll();
        return result;
    }

    @RequestMapping(
            value = "contrasena/nuevo",
            method = RequestMethod.POST,
            produces = "application/json"
    )
    public Object crear(@RequestBody Contrasenapago contra) {

        JSONObject js = new JSONObject();
        try {
            contra = rcp.save(contra);
            js.append("Exito", 1);
            return js.get("Exito");
        } catch (Exception e) {
            js.append("Error", 0);
            return js.get("Error");
        }

    }

    @RequestMapping(
            value = "contrasena/actualizar",
            method = RequestMethod.PUT,
            produces = "application/json"
    )
    public Object actualizar(@RequestBody Contrasenapago contra) {
        JSONObject js = new JSONObject();
        try {
            contra = rcp.save(contra);
            js.append("Exito", 1);
            return js.get("Exito");
        } catch (Exception e) {
            js.append("Error", 0);
            return js.get("Error");
        }
    }
    
    @RequestMapping(
            value ="contrasenapago/eliminar",
            method = RequestMethod.DELETE,
            produces = "application/json"
    )
    public Object eliminar (@RequestBody Contrasenapago contra){
        JSONObject js = new JSONObject();
        try {
            rcp.delete(contra);
            js.append("Exito", 1);
            return js.get("Exito");
        } catch (Exception e) {
            js.append("Error", 0);
            return js.get("Error");
        }
    }
}
