package com.backend.controles;

import com.backend.entidades.Controllote;
import com.backend.repositorios.repocontrollote;
import java.util.List;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author jhoan
 */
@RestController
@Service
@EnableJpaRepositories("com.backend.repositorios")
public class ControlControllote {

    @Autowired
    repocontrollote rcl;

    @CrossOrigin
    @RequestMapping(
            value ="controllote/all",
            method = RequestMethod.GET,
            produces = "applicaction/json"
    )
    public List<Controllote> getall() {
        List<Controllote> result = (List<Controllote>) rcl.findAll();

        return result;
    }
    
    @RequestMapping(
            value ="controllote/crear",
            method = RequestMethod.GET,
            produces = "application/json"
    )
    public Object crear (@RequestBody Controllote crtlote){
        
        JSONObject js = new JSONObject();
        try {
            crtlote = rcl.save(crtlote);
            js.append("Exito",1);
            return js.get("Exito");
        } catch (Exception e) {
            js.append("Error", 0);
            return js.get("Error");
        }
    }
    @RequestMapping(
            value = "controllote/actualizar",
            method = RequestMethod.PUT,
            produces = "application/json"
    )
    public Object actualizar (@RequestBody Controllote crtlote){
        JSONObject js = new JSONObject();
        try {
            crtlote = rcl.save(crtlote);
            js.append("Exito",1);
            return js.get("Exito");
        } catch (Exception e) {
            js.append("Error",1);
            return js.get("Error");
        }
    }
    
    @RequestMapping(
            value ="controllote/eliminar",
            method = RequestMethod.DELETE,
            produces = "application/json"
    )
    public Object eliminar (@RequestBody Controllote crtlote){
        JSONObject js = new JSONObject();
        try {
            rcl.delete(crtlote);
            js.append("Exito", 1);
            return js.get("Exito");
        } catch (Exception e) {
            js.append("Error",0);
            return js.get("Error");
        }
    }
}
