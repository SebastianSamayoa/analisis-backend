package com.backend.controles;

import com.backend.entidades.Facturaloteproveedor;
import com.backend.repositorios.repofacturaloteproveedor;
import java.util.List;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author jhoan
 */
@RestController
@Service
@EnableJpaRepositories("com.backend.repositorios")
public class ControlFacturaProveedorDetalle {

    @Autowired
    repofacturaloteproveedor rflp;

    @CrossOrigin
    @RequestMapping(
            value = "faclotepro/all",
            method = RequestMethod.GET,
            produces = "application/json"
    )
    public List<Facturaloteproveedor> getall() {
        List<Facturaloteproveedor> result = (List<Facturaloteproveedor>) rflp.findAll();

        return result;
    }
    @RequestMapping(
            value = "faclotepro/crear",
            method = RequestMethod.POST,
            produces = "application/json"
    )
    public Object crear (@RequestBody Facturaloteproveedor flp ){
    JSONObject js = new JSONObject();
        try {
            flp = rflp.save(flp);
            js.append("Exito", 1);
            return js.get("Exito");
        } catch (Exception e) {
            js.append("Error", 0);
            return js.get("Error");
        }
    }
    
    @RequestMapping(
            value = "faclotepro/actualizar",
            method = RequestMethod.PUT,
            produces = "application/json"
    )
    public Object actualizar(@RequestBody Facturaloteproveedor flp){
        JSONObject js = new JSONObject();
        try {
            flp = rflp.save(flp);
            js.append("Exito", 1);
            return js.get("Exito");
        } catch (Exception e) {
            js.append("Error", 0);
            return js.get("Error");
        }
    }
    
    @RequestMapping (
            value = "faclotepro/eliminar",
            method = RequestMethod.DELETE,
            produces = "application/json"
    )
    public Object eliminar (@RequestBody Facturaloteproveedor flp){
        JSONObject js = new JSONObject();
        try {
            rflp.delete(flp);
            js.append("Exito",1);
            return js.get("Exito");
        } catch (Exception e) {
            js.append("Error", 0);
            return js.get("Error");
        }
    }
}
