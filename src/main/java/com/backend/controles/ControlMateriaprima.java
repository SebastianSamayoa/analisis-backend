package com.backend.controles;

import com.backend.entidades.Materiaprima;
import com.backend.repositorios.repomateriaprima;
import java.util.List;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author jhoan
 */
@RestController
@Service
@EnableJpaRepositories("com.backend.repositorios")
public class ControlMateriaprima {

    @Autowired
    repomateriaprima rmp;

    @CrossOrigin
    @RequestMapping(
            value = "materiaprima/all",
            method = RequestMethod.GET,
            produces = "application/json"
    )
    public List<Materiaprima> getall() {
        List<Materiaprima> result = (List<Materiaprima>) rmp.findAll();
        return result;
    }

    @RequestMapping(
            value = "materiaprima/crear",
            method = RequestMethod.POST,
            produces = "application/json"
    )
    public Object crear(@RequestBody Materiaprima mp) {
        JSONObject js = new JSONObject();
        try {
            mp = rmp.save(mp);
            js.append("Exito", 1);
            return js.get("Exito");
        } catch (Exception e) {
            js.append("Error", 0);
            return js.get("Error");
        }

    }

    @RequestMapping(
            value = "materiaprima/actualizar",
            method = RequestMethod.PUT,
            produces = "application/json"
    )
    public Object actualizar(@RequestBody Materiaprima mp) {
        JSONObject js = new JSONObject();
        try {
            mp = rmp.save(mp);
            js.append("Exito", 1);
            return js.get("Exito");
        } catch (Exception e) {
            js.append("Error", 0);
            return js.get("Error");
        }
    }
    
    @RequestMapping(
            value = "materiaprima/eliminar",
            method = RequestMethod.DELETE,
            produces = "application/json"
    )
    public Object eliminar(@RequestBody Materiaprima mp) {
        JSONObject js = new JSONObject();
        try {
            rmp.delete(mp);
            js.append("Exito", 1);
            return js.get("Exito");
        } catch (Exception e) {
            js.append("Error", 0);
            return js.get("Error");
        }
    }

}
