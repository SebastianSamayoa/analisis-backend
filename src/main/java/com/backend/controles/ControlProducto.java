package com.backend.controles;

import com.backend.entidades.Producto;
import com.backend.repositorios.repoproducto;
import java.util.List;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author jhoan
 */
@RestController
@Service
@EnableJpaRepositories("com.backend.repositorios")
public class ControlProducto {

    @Autowired
    repoproducto rpro;

    @CrossOrigin
    @RequestMapping(
            value = "producto/all",
            method = RequestMethod.GET,
            produces = "application/json"
    )
    public List<Producto> getall() {
        List<Producto> result = (List<Producto>) rpro.findAll();
        return result;
    }

    @RequestMapping(
            value = "producto/crear",
            method = RequestMethod.POST,
            produces = "application/json"
    )
    public Object crear(@RequestBody Producto pd) {
        JSONObject js = new JSONObject();
        try {
            pd = rpro.save(pd);
            js.append("Exito", 1);
            return js.get("Exito");
        } catch (Exception e) {
            js.append("Error", 0);
            return js.get("Error");
        }
    }
    
    @RequestMapping(
            value = "producto/actualizar",
            method = RequestMethod.PUT,
            produces = "application/json"
    )
    public Object actualizar(@RequestBody Producto pd) {
        JSONObject js = new JSONObject();
        try {
            pd = rpro.save(pd);
            js.append("Exito", 1);
            return js.get("Exito");
        } catch (Exception e) {
            js.append("Error", 0);
            return js.get("Error");
        }
    }
    
    @RequestMapping(
            value = "producto/eliminar",
            method = RequestMethod.DELETE,
            produces = "application/json"
    )
    public Object eliminar(@RequestBody Producto pd) {
        JSONObject js = new JSONObject();
        try {
            rpro.delete(pd);
            js.append("Exito", 1);
            return js.get("Exito");
        } catch (Exception e) {
            js.append("Error", 0);
            return js.get("Error");
        }
    }
}
