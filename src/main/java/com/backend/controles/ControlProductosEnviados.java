package com.backend.controles;

import com.backend.entidades.Productosenviados;
import com.backend.repositorios.repoproductosenviados;
import java.util.List;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author jhoan
 */
@RestController
@Service
@EnableJpaRepositories("com.backend.repositorios")
public class ControlProductosEnviados {

    @Autowired
    repoproductosenviados rpe;

    @CrossOrigin
    @RequestMapping(
            value = "productosenviados/all",
            method = RequestMethod.GET,
            produces = "application/json"
    )
    public List<Productosenviados> getall() {
        List<Productosenviados> result = (List<Productosenviados>) rpe.findAll();
        return result;
    }

    @RequestMapping(
            value = "productosenviados/crear",
            method = RequestMethod.POST,
            produces = "application/json"
    )
    public Object crear(@RequestBody Productosenviados pe) {
        JSONObject js = new JSONObject();
        try {
            pe = rpe.save(pe);
            js.append("Exito", 1);
            return js.get("Exito");
        } catch (Exception e) {
            js.append("Error", 0);
            return js.get("Error");
        }
    }

    @RequestMapping(
            value = "productosenviados/actualizar",
            method = RequestMethod.PUT,
            produces = "application/json"
    )
    public Object actualizar(@RequestBody Productosenviados pe) {
        JSONObject js = new JSONObject();
        try {
            pe = rpe.save(pe);
            js.append("Exito", 1);
            return js.get("Exito");
        } catch (Exception e) {
            js.append("Error", 0);
            return js.get("Error");
        }
    }

    @RequestMapping(
            value = "productosenviados/eliminar",
            method = RequestMethod.DELETE,
            produces = "application/json"
    )
    public Object eliminar(@RequestBody Productosenviados pe) {
        JSONObject js = new JSONObject();
        try {
            rpe.delete(pe);
            js.append("Exito", 1);
            return js.get("Exito");
        } catch (Exception e) {
            js.append("Error", 0);
            return js.get("Error");
        }
    }
}
