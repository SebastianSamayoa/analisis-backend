package com.backend.controles;

import com.backend.entidades.Tipoacceso;
import com.backend.repositorios.repotipoacceso;
import java.util.List;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author jhoan
 */
@RestController
@Service
@EnableJpaRepositories("com.backend.repositorios")
public class ControlTipoacceso {

    @Autowired
    repotipoacceso rta;

    @CrossOrigin
    @RequestMapping(
            value = "tipoacceso/all",
            method = RequestMethod.GET,
            produces = "application/json"
    )
    public List<Tipoacceso> getall() {
        List<Tipoacceso> result = (List<Tipoacceso>) rta.findAll();
        return result;
    }

    @RequestMapping(
            value = "tipoacceso/crear",
            method = RequestMethod.POST,
            produces = "application/json"
    )
    public Object crear(@RequestBody Tipoacceso ta) {
        JSONObject js = new JSONObject();
        try {
            ta = rta.save(ta);
            js.append("Exito", 1);
            return js.get("Exito");
        } catch (Exception e) {
            js.append("Error", 0);
            return js.get("Error");
        }
    }

    @RequestMapping(
            value = "tipoacceso/actualizar",
            method = RequestMethod.PUT,
            produces = "application/json"
    )
    public Object actualizar(@RequestBody Tipoacceso ta) {
        JSONObject js = new JSONObject();
        try {
            ta = rta.save(ta);
            js.append("Exito", 1);
            return js.get("Exito");
        } catch (Exception e) {
            js.append("Error", 0);
            return js.get("Error");
        }
    }

    @RequestMapping(
            value = "tipoacceso/eliminar",
            method = RequestMethod.DELETE,
            produces = "application/json"
    )
    public Object eliminar(@RequestBody Tipoacceso ta) {
        JSONObject js = new JSONObject();
        try {
            rta.delete(ta);
            js.append("Exito", 1);
            return js.get("Exito");
        } catch (Exception e) {
            js.append("Error", 0);
            return js.get("Error");
        }
    }
}
