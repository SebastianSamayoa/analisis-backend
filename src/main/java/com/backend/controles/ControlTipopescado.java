package com.backend.controles;

import com.backend.entidades.Tipopescado;
import com.backend.repositorios.repotipopescado;
import java.util.List;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author jhoan
 */
@RestController
@Service
@EnableJpaRepositories("com.backend.repositorios")
public class ControlTipopescado {

    @Autowired
    repotipopescado rtp;

    @CrossOrigin
    @RequestMapping(
            value = "tipopescado/all",
            method = RequestMethod.GET,
            produces = "application/json"
    )
    public List<Tipopescado> getall() {
        List<Tipopescado> result = (List<Tipopescado>) rtp.findAll();
        return result;
    }

    @RequestMapping(
            value = "tipopescado/crear",
            method = RequestMethod.POST,
            produces = "application/json"
    )
    public Object crear(@RequestBody Tipopescado tp) {
        JSONObject js = new JSONObject();
        try {
            tp = rtp.save(tp);
            js.append("Exito", 1);
            return js.get("Exito");
        } catch (Exception e) {
            js.append("Error", 0);
            return js.get("Error");
        }
    }

    @RequestMapping(
            value = "tipopescado/actualizar",
            method = RequestMethod.PUT,
            produces = "application/json"
    )
    public Object actualizar(@RequestBody Tipopescado tp) {
        JSONObject js = new JSONObject();
        try {
            tp = rtp.save(tp);
            js.append("Exito", 1);
            return js.get("Exito");
        } catch (Exception e) {
            js.append("Error", 0);
            return js.get("Error");
        }
    }
    
        @RequestMapping(
            value = "tipopescado/eliminar",
            method = RequestMethod.DELETE,
            produces = "application/json"
    )
    public Object eliminar(@RequestBody Tipopescado tp) {
        JSONObject js = new JSONObject();
        try {
            rtp.delete(tp);
            js.append("Exito", 1);
            return js.get("Exito");
        } catch (Exception e) {
            js.append("Error", 0);
            return js.get("Error");
        }
    }
}
