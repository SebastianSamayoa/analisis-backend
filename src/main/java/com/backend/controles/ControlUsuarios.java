package com.backend.controles;

import com.backend.entidades.Usuarios;
import com.backend.repositorios.repousuarios;
import java.util.List;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author jhoan
 */
@RestController
@Service
@EnableJpaRepositories(basePackages = "com.backend.repositorios")
public class ControlUsuarios {

    @Autowired
    repousuarios ru;

    @CrossOrigin
    @RequestMapping(
            value = "usuarios/all",
            method = RequestMethod.GET,
            produces = "application/json"
    )
    public List<Usuarios> getall() {
        List<Usuarios> result = (List<Usuarios>) ru.findAll();

        return result;
    }

    @RequestMapping(
            value = "usuario/crear",
            method = RequestMethod.POST,
            produces = "application/json"
    )
    public Object crear(@RequestBody Usuarios u) {
        JSONObject js = new JSONObject();
        try {
            u = ru.save(u);
            js.append("Exito", 1);
            return js.get("Exito");
        } catch (Exception e) {
            js.append("Error", 0);
            return js.get("Error");
        }
    }

    @RequestMapping(
            value = "usuario/actualizar",
            method = RequestMethod.PUT,
            produces = "application/json"
    )
    public Object actualizar(@RequestBody Usuarios u) {
        JSONObject js = new JSONObject();
        try {
            u = ru.save(u);
            js.append("Exito", 1);
            return js.get("Exito");
        } catch (Exception e) {
            js.append("Error", 0);
            return js.get("Error");
        }
    }

    @RequestMapping(
            value = "usuario/eliminar",
            method = RequestMethod.DELETE,
            produces = "application/json"
    )
    public Object eliminar(@RequestBody Usuarios u) {
        JSONObject js = new JSONObject();
        try {
            ru.delete(u);
            js.append("Exito", 1);
            return js.get("Exito");
        } catch (Exception e) {
            js.append("Error", 0);
            return js.get("Error");
        }
    }
}
