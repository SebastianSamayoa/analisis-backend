package com.backend.controles;

import com.backend.entidades.Detalleingresomateriaprima;
import com.backend.repositorios.repodetingre;
import java.util.List;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author jhoan
 */
@RestController
@Service
@EnableJpaRepositories("com.backend.repositorios")
public class ControldetIngreso {

    @Autowired
    repodetingre rdi;

    @CrossOrigin
    @RequestMapping(
            value = "detingresomp/all",
            method = RequestMethod.GET,
            produces = "application/json"
    )
    public List<Detalleingresomateriaprima> getall() {
        List<Detalleingresomateriaprima> result = (List<Detalleingresomateriaprima>) rdi.findAll();
        return result;
    }

    @RequestMapping(
            value = "detingresomp/crear",
            method = RequestMethod.POST,
            produces = "application/json"
    )
    public Object crear(@RequestBody Detalleingresomateriaprima di) {
        JSONObject js = new JSONObject();
        try {
            di = rdi.save(di);
            js.append("Exito", 1);
            return js.get("Exito");
        } catch (Exception e) {
            js.append("Error", 0);
            return js.get("Error");
        }
    }

    @RequestMapping(
            value = "detingresomp/actualizar",
            method = RequestMethod.PUT,
            produces = "application/json"
    )
    public Object actualizar(@RequestBody Detalleingresomateriaprima di) {
        JSONObject js = new JSONObject();
        try {
            di = rdi.save(di);
            js.append("Exito", 1);
            return js.get("Exito");
        } catch (Exception e) {
            js.append("Error", 0);
            return js.get("Error");
        }
    }

    @RequestMapping(
            value = "detingresomp/eliminar",
            method = RequestMethod.DELETE,
            produces = "application/json"
    )
    public Object eliminar(@RequestBody Detalleingresomateriaprima di) {
        JSONObject js = new JSONObject();
        try {
            rdi.delete(di);
            js.append("Exito", 1);
            return js.get("Exito");
        } catch (Exception e) {
            js.append("Error", 0);
            return js.get("Error");
        }
    }
}
