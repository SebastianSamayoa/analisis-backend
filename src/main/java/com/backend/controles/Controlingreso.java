package com.backend.controles;

import com.backend.entidades.Ctringresomateriaprima;
import com.backend.repositorios.repoctring;
import java.util.List;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author jhoan
 */
@RestController
@Service
@EnableJpaRepositories("com.backend.repositorios")
public class Controlingreso {

    @Autowired
    repoctring rci;

    @CrossOrigin
    @RequestMapping(
            value = "controlingreso/all",
            method = RequestMethod.GET,
            produces = "application/json"
    )
    public List<Ctringresomateriaprima> getall() {
        List<Ctringresomateriaprima> result = (List<Ctringresomateriaprima>) rci.findAll();
        return result;
    }

    @RequestMapping(
            value = "controlingreso/crear",
            method = RequestMethod.POST,
            produces = "application/json"
    )
    public Object crear(@RequestBody Ctringresomateriaprima ci) {
        JSONObject js = new JSONObject();
        try {
            ci = rci.save(ci);
            js.append("Exito", 1);
            return js.get("Exito");
        } catch (Exception e) {
            js.append("Error", 0);
            return js.get("Error");
        }
    }

    @RequestMapping(
            value = "controlingreso/actualizar",
            method = RequestMethod.PUT,
            produces = "application/json"
    )
    public Object actualizar(@RequestBody Ctringresomateriaprima ci) {
        JSONObject js = new JSONObject();
        try {
            ci = rci.save(ci);
            js.append("Exito", 1);
            return js.get("Exito");
        } catch (Exception e) {
            js.append("Error", 0);
            return js.get("Error");
        }
    }

    @RequestMapping(
            value = "controlingreso/eliminar",
            method = RequestMethod.POST,
            produces = "application/json"
    )
    public Object eliminar(@RequestBody Ctringresomateriaprima ci) {
        JSONObject js = new JSONObject();
        try {
            rci.delete(ci);
            js.append("Exito", 1);
            return js.get("Exito");
        } catch (Exception e) {
            js.append("Error", 0);
            return js.get("Error");
        }
    }
}
