/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jhoan
 */
@Entity
@Table(name = "CALIDADPRODUCTO", catalog = "", schema = "ANALISIS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Calidadproducto.findAll", query = "SELECT c FROM Calidadproducto c"),
    @NamedQuery(name = "Calidadproducto.findByIdcalidadproducto", query = "SELECT c FROM Calidadproducto c WHERE c.idcalidadproducto = :idcalidadproducto"),
    @NamedQuery(name = "Calidadproducto.findByCalidadproducto", query = "SELECT c FROM Calidadproducto c WHERE c.calidadproducto = :calidadproducto")})
public class Calidadproducto implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "IDCALIDADPRODUCTO")
    private BigDecimal idcalidadproducto;
    @Size(max = 20)
    @Column(name = "CALIDADPRODUCTO")
    private String calidadproducto;

    public Calidadproducto() {
    }

    public Calidadproducto(BigDecimal idcalidadproducto) {
        this.idcalidadproducto = idcalidadproducto;
    }

    public BigDecimal getIdcalidadproducto() {
        return idcalidadproducto;
    }

    public void setIdcalidadproducto(BigDecimal idcalidadproducto) {
        this.idcalidadproducto = idcalidadproducto;
    }

    public String getCalidadproducto() {
        return calidadproducto;
    }

    public void setCalidadproducto(String calidadproducto) {
        this.calidadproducto = calidadproducto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idcalidadproducto != null ? idcalidadproducto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Calidadproducto)) {
            return false;
        }
        Calidadproducto other = (Calidadproducto) object;
        if ((this.idcalidadproducto == null && other.idcalidadproducto != null) || (this.idcalidadproducto != null && !this.idcalidadproducto.equals(other.idcalidadproducto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.backend.entidades.Calidadproducto[ idcalidadproducto=" + idcalidadproducto + " ]";
    }
    
}
