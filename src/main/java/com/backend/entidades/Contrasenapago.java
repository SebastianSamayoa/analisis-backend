/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jhoan
 */
@Entity
@Table(name = "CONTRASENAPAGO", catalog = "", schema = "ANALISIS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Contrasenapago.findAll", query = "SELECT c FROM Contrasenapago c"),
    @NamedQuery(name = "Contrasenapago.findByIdcontrasenia", query = "SELECT c FROM Contrasenapago c WHERE c.idcontrasenia = :idcontrasenia"),
    @NamedQuery(name = "Contrasenapago.findByIdfacturaproveedor", query = "SELECT c FROM Contrasenapago c WHERE c.idfacturaproveedor = :idfacturaproveedor"),
    @NamedQuery(name = "Contrasenapago.findByFechapago", query = "SELECT c FROM Contrasenapago c WHERE c.fechapago = :fechapago")})
public class Contrasenapago implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "IDCONTRASENIA")
    private BigDecimal idcontrasenia;
    @Column(name = "IDFACTURAPROVEEDOR")
    private BigInteger idfacturaproveedor;
    @Column(name = "FECHAPAGO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechapago;

    public Contrasenapago() {
    }

    public Contrasenapago(BigDecimal idcontrasenia) {
        this.idcontrasenia = idcontrasenia;
    }

    public BigDecimal getIdcontrasenia() {
        return idcontrasenia;
    }

    public void setIdcontrasenia(BigDecimal idcontrasenia) {
        this.idcontrasenia = idcontrasenia;
    }

    public BigInteger getIdfacturaproveedor() {
        return idfacturaproveedor;
    }

    public void setIdfacturaproveedor(BigInteger idfacturaproveedor) {
        this.idfacturaproveedor = idfacturaproveedor;
    }

    public Date getFechapago() {
        return fechapago;
    }

    public void setFechapago(Date fechapago) {
        this.fechapago = fechapago;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idcontrasenia != null ? idcontrasenia.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Contrasenapago)) {
            return false;
        }
        Contrasenapago other = (Contrasenapago) object;
        if ((this.idcontrasenia == null && other.idcontrasenia != null) || (this.idcontrasenia != null && !this.idcontrasenia.equals(other.idcontrasenia))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.backend.entidades.Contrasenapago[ idcontrasenia=" + idcontrasenia + " ]";
    }
    
}
