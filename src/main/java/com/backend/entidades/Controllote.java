/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jhoan
 */
@Entity
@Table(name = "CONTROLLOTE", catalog = "", schema = "ANALISIS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Controllote.findAll", query = "SELECT c FROM Controllote c"),
    @NamedQuery(name = "Controllote.findByIdcontrollote", query = "SELECT c FROM Controllote c WHERE c.idcontrollote = :idcontrollote"),
    @NamedQuery(name = "Controllote.findByIdcontrol", query = "SELECT c FROM Controllote c WHERE c.idcontrol = :idcontrol"),
    @NamedQuery(name = "Controllote.findByIdestado", query = "SELECT c FROM Controllote c WHERE c.idestado = :idestado"),
    @NamedQuery(name = "Controllote.findByIdproceso", query = "SELECT c FROM Controllote c WHERE c.idproceso = :idproceso")})
public class Controllote implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "IDCONTROLLOTE")
    private BigDecimal idcontrollote;
    @Column(name = "IDCONTROL")
    private BigInteger idcontrol;
    @Column(name = "IDESTADO")
    private BigInteger idestado;
    @Column(name = "IDPROCESO")
    private BigInteger idproceso;

    public Controllote() {
    }

    public Controllote(BigDecimal idcontrollote) {
        this.idcontrollote = idcontrollote;
    }

    public BigDecimal getIdcontrollote() {
        return idcontrollote;
    }

    public void setIdcontrollote(BigDecimal idcontrollote) {
        this.idcontrollote = idcontrollote;
    }

    public BigInteger getIdcontrol() {
        return idcontrol;
    }

    public void setIdcontrol(BigInteger idcontrol) {
        this.idcontrol = idcontrol;
    }

    public BigInteger getIdestado() {
        return idestado;
    }

    public void setIdestado(BigInteger idestado) {
        this.idestado = idestado;
    }

    public BigInteger getIdproceso() {
        return idproceso;
    }

    public void setIdproceso(BigInteger idproceso) {
        this.idproceso = idproceso;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idcontrollote != null ? idcontrollote.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Controllote)) {
            return false;
        }
        Controllote other = (Controllote) object;
        if ((this.idcontrollote == null && other.idcontrollote != null) || (this.idcontrollote != null && !this.idcontrollote.equals(other.idcontrollote))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.backend.entidades.Controllote[ idcontrollote=" + idcontrollote + " ]";
    }
    
}
