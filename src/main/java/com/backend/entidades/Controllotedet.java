/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jhoan
 */
@Entity
@Table(name = "CONTROLLOTEDET", catalog = "", schema = "ANALISIS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Controllotedet.findAll", query = "SELECT c FROM Controllotedet c"),
    @NamedQuery(name = "Controllotedet.findByIdlotedetalle", query = "SELECT c FROM Controllotedet c WHERE c.idlotedetalle = :idlotedetalle"),
    @NamedQuery(name = "Controllotedet.findByIdcontrollote", query = "SELECT c FROM Controllotedet c WHERE c.idcontrollote = :idcontrollote"),
    @NamedQuery(name = "Controllotedet.findByIdproceso", query = "SELECT c FROM Controllotedet c WHERE c.idproceso = :idproceso"),
    @NamedQuery(name = "Controllotedet.findByIdusuario", query = "SELECT c FROM Controllotedet c WHERE c.idusuario = :idusuario"),
    @NamedQuery(name = "Controllotedet.findByFechacontrol", query = "SELECT c FROM Controllotedet c WHERE c.fechacontrol = :fechacontrol")})
public class Controllotedet implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "IDLOTEDETALLE")
    private BigDecimal idlotedetalle;
    @Column(name = "IDCONTROLLOTE")
    private BigInteger idcontrollote;
    @Column(name = "IDPROCESO")
    private BigInteger idproceso;
    @Column(name = "IDUSUARIO")
    private BigInteger idusuario;
    @Column(name = "FECHACONTROL")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechacontrol;

    public Controllotedet() {
    }

    public Controllotedet(BigDecimal idlotedetalle) {
        this.idlotedetalle = idlotedetalle;
    }

    public BigDecimal getIdlotedetalle() {
        return idlotedetalle;
    }

    public void setIdlotedetalle(BigDecimal idlotedetalle) {
        this.idlotedetalle = idlotedetalle;
    }

    public BigInteger getIdcontrollote() {
        return idcontrollote;
    }

    public void setIdcontrollote(BigInteger idcontrollote) {
        this.idcontrollote = idcontrollote;
    }

    public BigInteger getIdproceso() {
        return idproceso;
    }

    public void setIdproceso(BigInteger idproceso) {
        this.idproceso = idproceso;
    }

    public BigInteger getIdusuario() {
        return idusuario;
    }

    public void setIdusuario(BigInteger idusuario) {
        this.idusuario = idusuario;
    }

    public Date getFechacontrol() {
        return fechacontrol;
    }

    public void setFechacontrol(Date fechacontrol) {
        this.fechacontrol = fechacontrol;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idlotedetalle != null ? idlotedetalle.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Controllotedet)) {
            return false;
        }
        Controllotedet other = (Controllotedet) object;
        if ((this.idlotedetalle == null && other.idlotedetalle != null) || (this.idlotedetalle != null && !this.idlotedetalle.equals(other.idlotedetalle))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.backend.entidades.Controllotedet[ idlotedetalle=" + idlotedetalle + " ]";
    }
    
}
