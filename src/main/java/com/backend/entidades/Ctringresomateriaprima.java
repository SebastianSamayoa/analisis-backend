/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jhoan
 */
@Entity
@Table(name = "CTRINGRESOMATERIAPRIMA", catalog = "", schema = "ANALISIS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ctringresomateriaprima.findAll", query = "SELECT c FROM Ctringresomateriaprima c"),
    @NamedQuery(name = "Ctringresomateriaprima.findByIdcontrol", query = "SELECT c FROM Ctringresomateriaprima c WHERE c.idcontrol = :idcontrol"),
    @NamedQuery(name = "Ctringresomateriaprima.findByIdproveedor", query = "SELECT c FROM Ctringresomateriaprima c WHERE c.idproveedor = :idproveedor"),
    @NamedQuery(name = "Ctringresomateriaprima.findByIdestado", query = "SELECT c FROM Ctringresomateriaprima c WHERE c.idestado = :idestado"),
    @NamedQuery(name = "Ctringresomateriaprima.findByIdloteproveedor", query = "SELECT c FROM Ctringresomateriaprima c WHERE c.idloteproveedor = :idloteproveedor"),
    @NamedQuery(name = "Ctringresomateriaprima.findByFecharecepcion", query = "SELECT c FROM Ctringresomateriaprima c WHERE c.fecharecepcion = :fecharecepcion")})
public class Ctringresomateriaprima implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "IDCONTROL")
    private BigDecimal idcontrol;
    @Column(name = "IDPROVEEDOR")
    private BigInteger idproveedor;
    @Column(name = "IDESTADO")
    private BigInteger idestado;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IDLOTEPROVEEDOR")
    private BigInteger idloteproveedor;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHARECEPCION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecharecepcion;

    public Ctringresomateriaprima() {
    }

    public Ctringresomateriaprima(BigDecimal idcontrol) {
        this.idcontrol = idcontrol;
    }

    public Ctringresomateriaprima(BigDecimal idcontrol, BigInteger idloteproveedor, Date fecharecepcion) {
        this.idcontrol = idcontrol;
        this.idloteproveedor = idloteproveedor;
        this.fecharecepcion = fecharecepcion;
    }

    public BigDecimal getIdcontrol() {
        return idcontrol;
    }

    public void setIdcontrol(BigDecimal idcontrol) {
        this.idcontrol = idcontrol;
    }

    public BigInteger getIdproveedor() {
        return idproveedor;
    }

    public void setIdproveedor(BigInteger idproveedor) {
        this.idproveedor = idproveedor;
    }

    public BigInteger getIdestado() {
        return idestado;
    }

    public void setIdestado(BigInteger idestado) {
        this.idestado = idestado;
    }

    public BigInteger getIdloteproveedor() {
        return idloteproveedor;
    }

    public void setIdloteproveedor(BigInteger idloteproveedor) {
        this.idloteproveedor = idloteproveedor;
    }

    public Date getFecharecepcion() {
        return fecharecepcion;
    }

    public void setFecharecepcion(Date fecharecepcion) {
        this.fecharecepcion = fecharecepcion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idcontrol != null ? idcontrol.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ctringresomateriaprima)) {
            return false;
        }
        Ctringresomateriaprima other = (Ctringresomateriaprima) object;
        if ((this.idcontrol == null && other.idcontrol != null) || (this.idcontrol != null && !this.idcontrol.equals(other.idcontrol))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.backend.entidades.Ctringresomateriaprima[ idcontrol=" + idcontrol + " ]";
    }
    
}
