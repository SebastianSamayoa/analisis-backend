/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jhoan
 */
@Entity
@Table(name = "DETALLEINGRESOMATERIAPRIMA", catalog = "", schema = "ANALISIS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Detalleingresomateriaprima.findAll", query = "SELECT d FROM Detalleingresomateriaprima d"),
    @NamedQuery(name = "Detalleingresomateriaprima.findByIddetalle", query = "SELECT d FROM Detalleingresomateriaprima d WHERE d.iddetalle = :iddetalle"),
    @NamedQuery(name = "Detalleingresomateriaprima.findByIdcontrol", query = "SELECT d FROM Detalleingresomateriaprima d WHERE d.idcontrol = :idcontrol"),
    @NamedQuery(name = "Detalleingresomateriaprima.findByIdmateriaprima", query = "SELECT d FROM Detalleingresomateriaprima d WHERE d.idmateriaprima = :idmateriaprima"),
    @NamedQuery(name = "Detalleingresomateriaprima.findByIdtipopescado", query = "SELECT d FROM Detalleingresomateriaprima d WHERE d.idtipopescado = :idtipopescado"),
    @NamedQuery(name = "Detalleingresomateriaprima.findByIdcalidadproducto", query = "SELECT d FROM Detalleingresomateriaprima d WHERE d.idcalidadproducto = :idcalidadproducto"),
    @NamedQuery(name = "Detalleingresomateriaprima.findByIdusuario", query = "SELECT d FROM Detalleingresomateriaprima d WHERE d.idusuario = :idusuario"),
    @NamedQuery(name = "Detalleingresomateriaprima.findByCantidad", query = "SELECT d FROM Detalleingresomateriaprima d WHERE d.cantidad = :cantidad"),
    @NamedQuery(name = "Detalleingresomateriaprima.findByPesolbs", query = "SELECT d FROM Detalleingresomateriaprima d WHERE d.pesolbs = :pesolbs")})
public class Detalleingresomateriaprima implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "IDDETALLE")
    private BigDecimal iddetalle;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IDCONTROL")
    private BigInteger idcontrol;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IDMATERIAPRIMA")
    private BigInteger idmateriaprima;
    @Column(name = "IDTIPOPESCADO")
    private BigInteger idtipopescado;
    @Column(name = "IDCALIDADPRODUCTO")
    private BigInteger idcalidadproducto;
    @Column(name = "IDUSUARIO")
    private BigInteger idusuario;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CANTIDAD")
    private int cantidad;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PESOLBS")
    private BigDecimal pesolbs;

    public Detalleingresomateriaprima() {
    }

    public Detalleingresomateriaprima(BigDecimal iddetalle) {
        this.iddetalle = iddetalle;
    }

    public Detalleingresomateriaprima(BigDecimal iddetalle, BigInteger idcontrol, BigInteger idmateriaprima, int cantidad, BigDecimal pesolbs) {
        this.iddetalle = iddetalle;
        this.idcontrol = idcontrol;
        this.idmateriaprima = idmateriaprima;
        this.cantidad = cantidad;
        this.pesolbs = pesolbs;
    }

    public BigDecimal getIddetalle() {
        return iddetalle;
    }

    public void setIddetalle(BigDecimal iddetalle) {
        this.iddetalle = iddetalle;
    }

    public BigInteger getIdcontrol() {
        return idcontrol;
    }

    public void setIdcontrol(BigInteger idcontrol) {
        this.idcontrol = idcontrol;
    }

    public BigInteger getIdmateriaprima() {
        return idmateriaprima;
    }

    public void setIdmateriaprima(BigInteger idmateriaprima) {
        this.idmateriaprima = idmateriaprima;
    }

    public BigInteger getIdtipopescado() {
        return idtipopescado;
    }

    public void setIdtipopescado(BigInteger idtipopescado) {
        this.idtipopescado = idtipopescado;
    }

    public BigInteger getIdcalidadproducto() {
        return idcalidadproducto;
    }

    public void setIdcalidadproducto(BigInteger idcalidadproducto) {
        this.idcalidadproducto = idcalidadproducto;
    }

    public BigInteger getIdusuario() {
        return idusuario;
    }

    public void setIdusuario(BigInteger idusuario) {
        this.idusuario = idusuario;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public BigDecimal getPesolbs() {
        return pesolbs;
    }

    public void setPesolbs(BigDecimal pesolbs) {
        this.pesolbs = pesolbs;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (iddetalle != null ? iddetalle.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Detalleingresomateriaprima)) {
            return false;
        }
        Detalleingresomateriaprima other = (Detalleingresomateriaprima) object;
        if ((this.iddetalle == null && other.iddetalle != null) || (this.iddetalle != null && !this.iddetalle.equals(other.iddetalle))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.backend.entidades.Detalleingresomateriaprima[ iddetalle=" + iddetalle + " ]";
    }
    
}
