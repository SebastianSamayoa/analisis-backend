/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jhoan
 */
@Entity
@Table(name = "ESTADOS", catalog = "", schema = "ANALISIS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Estados.findAll", query = "SELECT e FROM Estados e"),
    @NamedQuery(name = "Estados.findByIdestado", query = "SELECT e FROM Estados e WHERE e.idestado = :idestado"),
    @NamedQuery(name = "Estados.findByEstado", query = "SELECT e FROM Estados e WHERE e.estado = :estado"),
    @NamedQuery(name = "Estados.findByTipocatalogo", query = "SELECT e FROM Estados e WHERE e.tipocatalogo = :tipocatalogo")})
public class Estados implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "IDESTADO")
    private BigDecimal idestado;
    @Size(max = 30)
    @Column(name = "ESTADO")
    private String estado;
    @Column(name = "TIPOCATALOGO")
    private Character tipocatalogo;

    public Estados() {
    }

    public Estados(BigDecimal idestado) {
        this.idestado = idestado;
    }

    public BigDecimal getIdestado() {
        return idestado;
    }

    public void setIdestado(BigDecimal idestado) {
        this.idestado = idestado;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Character getTipocatalogo() {
        return tipocatalogo;
    }

    public void setTipocatalogo(Character tipocatalogo) {
        this.tipocatalogo = tipocatalogo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idestado != null ? idestado.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Estados)) {
            return false;
        }
        Estados other = (Estados) object;
        if ((this.idestado == null && other.idestado != null) || (this.idestado != null && !this.idestado.equals(other.idestado))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.backend.entidades.Estados[ idestado=" + idestado + " ]";
    }
    
}
