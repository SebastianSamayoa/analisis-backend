/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jhoan
 */
@Entity
@Table(name = "FACTURALOTEPROVEEDOR", catalog = "", schema = "ANALISIS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Facturaloteproveedor.findAll", query = "SELECT f FROM Facturaloteproveedor f"),
    @NamedQuery(name = "Facturaloteproveedor.findByIdfacturaloteprov", query = "SELECT f FROM Facturaloteproveedor f WHERE f.idfacturaloteprov = :idfacturaloteprov"),
    @NamedQuery(name = "Facturaloteproveedor.findByIdfacturaproveedor", query = "SELECT f FROM Facturaloteproveedor f WHERE f.idfacturaproveedor = :idfacturaproveedor"),
    @NamedQuery(name = "Facturaloteproveedor.findByIdloteproveedor", query = "SELECT f FROM Facturaloteproveedor f WHERE f.idloteproveedor = :idloteproveedor")})
public class Facturaloteproveedor implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "IDFACTURALOTEPROV")
    private BigDecimal idfacturaloteprov;
    @Column(name = "IDFACTURAPROVEEDOR")
    private BigInteger idfacturaproveedor;
    @Column(name = "IDLOTEPROVEEDOR")
    private BigInteger idloteproveedor;

    public Facturaloteproveedor() {
    }

    public Facturaloteproveedor(BigDecimal idfacturaloteprov) {
        this.idfacturaloteprov = idfacturaloteprov;
    }

    public BigDecimal getIdfacturaloteprov() {
        return idfacturaloteprov;
    }

    public void setIdfacturaloteprov(BigDecimal idfacturaloteprov) {
        this.idfacturaloteprov = idfacturaloteprov;
    }

    public BigInteger getIdfacturaproveedor() {
        return idfacturaproveedor;
    }

    public void setIdfacturaproveedor(BigInteger idfacturaproveedor) {
        this.idfacturaproveedor = idfacturaproveedor;
    }

    public BigInteger getIdloteproveedor() {
        return idloteproveedor;
    }

    public void setIdloteproveedor(BigInteger idloteproveedor) {
        this.idloteproveedor = idloteproveedor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idfacturaloteprov != null ? idfacturaloteprov.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Facturaloteproveedor)) {
            return false;
        }
        Facturaloteproveedor other = (Facturaloteproveedor) object;
        if ((this.idfacturaloteprov == null && other.idfacturaloteprov != null) || (this.idfacturaloteprov != null && !this.idfacturaloteprov.equals(other.idfacturaloteprov))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.backend.entidades.Facturaloteproveedor[ idfacturaloteprov=" + idfacturaloteprov + " ]";
    }
    
}
