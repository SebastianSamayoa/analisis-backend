/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jhoan
 */
@Entity
@Table(name = "FACTURAPROVEEDOR", catalog = "", schema = "ANALISIS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Facturaproveedor.findAll", query = "SELECT f FROM Facturaproveedor f"),
    @NamedQuery(name = "Facturaproveedor.findByIdfacturaproveedor", query = "SELECT f FROM Facturaproveedor f WHERE f.idfacturaproveedor = :idfacturaproveedor"),
    @NamedQuery(name = "Facturaproveedor.findByIdproveedor", query = "SELECT f FROM Facturaproveedor f WHERE f.idproveedor = :idproveedor"),
    @NamedQuery(name = "Facturaproveedor.findByNumerofactura", query = "SELECT f FROM Facturaproveedor f WHERE f.numerofactura = :numerofactura"),
    @NamedQuery(name = "Facturaproveedor.findBySerie", query = "SELECT f FROM Facturaproveedor f WHERE f.serie = :serie"),
    @NamedQuery(name = "Facturaproveedor.findByMonto", query = "SELECT f FROM Facturaproveedor f WHERE f.monto = :monto")})
public class Facturaproveedor implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "IDFACTURAPROVEEDOR")
    private BigDecimal idfacturaproveedor;
    @Column(name = "IDPROVEEDOR")
    private BigInteger idproveedor;
    @Column(name = "NUMEROFACTURA")
    private BigInteger numerofactura;
    @Size(max = 5)
    @Column(name = "SERIE")
    private String serie;
    @Column(name = "MONTO")
    private BigDecimal monto;

    public Facturaproveedor() {
    }

    public Facturaproveedor(BigDecimal idfacturaproveedor) {
        this.idfacturaproveedor = idfacturaproveedor;
    }

    public BigDecimal getIdfacturaproveedor() {
        return idfacturaproveedor;
    }

    public void setIdfacturaproveedor(BigDecimal idfacturaproveedor) {
        this.idfacturaproveedor = idfacturaproveedor;
    }

    public BigInteger getIdproveedor() {
        return idproveedor;
    }

    public void setIdproveedor(BigInteger idproveedor) {
        this.idproveedor = idproveedor;
    }

    public BigInteger getNumerofactura() {
        return numerofactura;
    }

    public void setNumerofactura(BigInteger numerofactura) {
        this.numerofactura = numerofactura;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public BigDecimal getMonto() {
        return monto;
    }

    public void setMonto(BigDecimal monto) {
        this.monto = monto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idfacturaproveedor != null ? idfacturaproveedor.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Facturaproveedor)) {
            return false;
        }
        Facturaproveedor other = (Facturaproveedor) object;
        if ((this.idfacturaproveedor == null && other.idfacturaproveedor != null) || (this.idfacturaproveedor != null && !this.idfacturaproveedor.equals(other.idfacturaproveedor))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.backend.entidades.Facturaproveedor[ idfacturaproveedor=" + idfacturaproveedor + " ]";
    }
    
}
