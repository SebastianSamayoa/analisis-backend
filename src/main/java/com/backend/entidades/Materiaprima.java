/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jhoan
 */
@Entity
@Table(name = "MATERIAPRIMA", catalog = "", schema = "ANALISIS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Materiaprima.findAll", query = "SELECT m FROM Materiaprima m"),
    @NamedQuery(name = "Materiaprima.findByIdmateriaprima", query = "SELECT m FROM Materiaprima m WHERE m.idmateriaprima = :idmateriaprima"),
    @NamedQuery(name = "Materiaprima.findByMateriaprima", query = "SELECT m FROM Materiaprima m WHERE m.materiaprima = :materiaprima")})
public class Materiaprima implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "IDMATERIAPRIMA")
    private BigDecimal idmateriaprima;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "MATERIAPRIMA")
    private String materiaprima;

    public Materiaprima() {
    }

    public Materiaprima(BigDecimal idmateriaprima) {
        this.idmateriaprima = idmateriaprima;
    }

    public Materiaprima(BigDecimal idmateriaprima, String materiaprima) {
        this.idmateriaprima = idmateriaprima;
        this.materiaprima = materiaprima;
    }

    public BigDecimal getIdmateriaprima() {
        return idmateriaprima;
    }

    public void setIdmateriaprima(BigDecimal idmateriaprima) {
        this.idmateriaprima = idmateriaprima;
    }

    public String getMateriaprima() {
        return materiaprima;
    }

    public void setMateriaprima(String materiaprima) {
        this.materiaprima = materiaprima;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idmateriaprima != null ? idmateriaprima.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Materiaprima)) {
            return false;
        }
        Materiaprima other = (Materiaprima) object;
        if ((this.idmateriaprima == null && other.idmateriaprima != null) || (this.idmateriaprima != null && !this.idmateriaprima.equals(other.idmateriaprima))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.backend.entidades.Materiaprima[ idmateriaprima=" + idmateriaprima + " ]";
    }
    
}
