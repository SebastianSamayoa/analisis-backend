/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jhoan
 */
@Entity
@Table(name = "PRODUCTO", catalog = "", schema = "ANALISIS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Producto.findAll", query = "SELECT p FROM Producto p"),
    @NamedQuery(name = "Producto.findByIdproducto", query = "SELECT p FROM Producto p WHERE p.idproducto = :idproducto"),
    @NamedQuery(name = "Producto.findByIdcontrollote", query = "SELECT p FROM Producto p WHERE p.idcontrollote = :idcontrollote"),
    @NamedQuery(name = "Producto.findByFechavence", query = "SELECT p FROM Producto p WHERE p.fechavence = :fechavence"),
    @NamedQuery(name = "Producto.findByIdestado", query = "SELECT p FROM Producto p WHERE p.idestado = :idestado")})
public class Producto implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "IDPRODUCTO")
    private BigDecimal idproducto;
    @Column(name = "IDCONTROLLOTE")
    private BigInteger idcontrollote;
    @Column(name = "FECHAVENCE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechavence;
    @Column(name = "IDESTADO")
    private BigInteger idestado;

    public Producto() {
    }

    public Producto(BigDecimal idproducto) {
        this.idproducto = idproducto;
    }

    public BigDecimal getIdproducto() {
        return idproducto;
    }

    public void setIdproducto(BigDecimal idproducto) {
        this.idproducto = idproducto;
    }

    public BigInteger getIdcontrollote() {
        return idcontrollote;
    }

    public void setIdcontrollote(BigInteger idcontrollote) {
        this.idcontrollote = idcontrollote;
    }

    public Date getFechavence() {
        return fechavence;
    }

    public void setFechavence(Date fechavence) {
        this.fechavence = fechavence;
    }

    public BigInteger getIdestado() {
        return idestado;
    }

    public void setIdestado(BigInteger idestado) {
        this.idestado = idestado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idproducto != null ? idproducto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Producto)) {
            return false;
        }
        Producto other = (Producto) object;
        if ((this.idproducto == null && other.idproducto != null) || (this.idproducto != null && !this.idproducto.equals(other.idproducto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.backend.entidades.Producto[ idproducto=" + idproducto + " ]";
    }
    
}
