/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jhoan
 */
@Entity
@Table(name = "PRODUCTOSENVIADOS", catalog = "", schema = "ANALISIS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Productosenviados.findAll", query = "SELECT p FROM Productosenviados p"),
    @NamedQuery(name = "Productosenviados.findByIdproductoenviado", query = "SELECT p FROM Productosenviados p WHERE p.idproductoenviado = :idproductoenviado"),
    @NamedQuery(name = "Productosenviados.findByIdproducto", query = "SELECT p FROM Productosenviados p WHERE p.idproducto = :idproducto"),
    @NamedQuery(name = "Productosenviados.findByFechaenvio", query = "SELECT p FROM Productosenviados p WHERE p.fechaenvio = :fechaenvio")})
public class Productosenviados implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "IDPRODUCTOENVIADO")
    private BigDecimal idproductoenviado;
    @Column(name = "IDPRODUCTO")
    private BigInteger idproducto;
    @Column(name = "FECHAENVIO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaenvio;

    public Productosenviados() {
    }

    public Productosenviados(BigDecimal idproductoenviado) {
        this.idproductoenviado = idproductoenviado;
    }

    public BigDecimal getIdproductoenviado() {
        return idproductoenviado;
    }

    public void setIdproductoenviado(BigDecimal idproductoenviado) {
        this.idproductoenviado = idproductoenviado;
    }

    public BigInteger getIdproducto() {
        return idproducto;
    }

    public void setIdproducto(BigInteger idproducto) {
        this.idproducto = idproducto;
    }

    public Date getFechaenvio() {
        return fechaenvio;
    }

    public void setFechaenvio(Date fechaenvio) {
        this.fechaenvio = fechaenvio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idproductoenviado != null ? idproductoenviado.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Productosenviados)) {
            return false;
        }
        Productosenviados other = (Productosenviados) object;
        if ((this.idproductoenviado == null && other.idproductoenviado != null) || (this.idproductoenviado != null && !this.idproductoenviado.equals(other.idproductoenviado))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.backend.entidades.Productosenviados[ idproductoenviado=" + idproductoenviado + " ]";
    }
    
}
