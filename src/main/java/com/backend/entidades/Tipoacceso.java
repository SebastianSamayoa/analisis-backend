/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jhoan
 */
@Entity
@Table(name = "TIPOACCESO", catalog = "", schema = "ANALISIS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tipoacceso.findAll", query = "SELECT t FROM Tipoacceso t"),
    @NamedQuery(name = "Tipoacceso.findByIdtipoacceso", query = "SELECT t FROM Tipoacceso t WHERE t.idtipoacceso = :idtipoacceso"),
    @NamedQuery(name = "Tipoacceso.findByTipoacceso", query = "SELECT t FROM Tipoacceso t WHERE t.tipoacceso = :tipoacceso")})
public class Tipoacceso implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "IDTIPOACCESO")
    private BigDecimal idtipoacceso;
    @Size(max = 10)
    @Column(name = "TIPOACCESO")
    private String tipoacceso;

    public Tipoacceso() {
    }

    public Tipoacceso(BigDecimal idtipoacceso) {
        this.idtipoacceso = idtipoacceso;
    }

    public BigDecimal getIdtipoacceso() {
        return idtipoacceso;
    }

    public void setIdtipoacceso(BigDecimal idtipoacceso) {
        this.idtipoacceso = idtipoacceso;
    }

    public String getTipoacceso() {
        return tipoacceso;
    }

    public void setTipoacceso(String tipoacceso) {
        this.tipoacceso = tipoacceso;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idtipoacceso != null ? idtipoacceso.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tipoacceso)) {
            return false;
        }
        Tipoacceso other = (Tipoacceso) object;
        if ((this.idtipoacceso == null && other.idtipoacceso != null) || (this.idtipoacceso != null && !this.idtipoacceso.equals(other.idtipoacceso))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.backend.entidades.Tipoacceso[ idtipoacceso=" + idtipoacceso + " ]";
    }
    
}
