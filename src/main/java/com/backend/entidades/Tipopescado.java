/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jhoan
 */
@Entity
@Table(name = "TIPOPESCADO", catalog = "", schema = "ANALISIS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tipopescado.findAll", query = "SELECT t FROM Tipopescado t"),
    @NamedQuery(name = "Tipopescado.findByIdtipopescado", query = "SELECT t FROM Tipopescado t WHERE t.idtipopescado = :idtipopescado"),
    @NamedQuery(name = "Tipopescado.findByTipopescado", query = "SELECT t FROM Tipopescado t WHERE t.tipopescado = :tipopescado")})
public class Tipopescado implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "IDTIPOPESCADO")
    private BigDecimal idtipopescado;
    @Size(max = 50)
    @Column(name = "TIPOPESCADO")
    private String tipopescado;

    public Tipopescado() {
    }

    public Tipopescado(BigDecimal idtipopescado) {
        this.idtipopescado = idtipopescado;
    }

    public BigDecimal getIdtipopescado() {
        return idtipopescado;
    }

    public void setIdtipopescado(BigDecimal idtipopescado) {
        this.idtipopescado = idtipopescado;
    }

    public String getTipopescado() {
        return tipopescado;
    }

    public void setTipopescado(String tipopescado) {
        this.tipopescado = tipopescado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idtipopescado != null ? idtipopescado.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tipopescado)) {
            return false;
        }
        Tipopescado other = (Tipopescado) object;
        if ((this.idtipopescado == null && other.idtipopescado != null) || (this.idtipopescado != null && !this.idtipopescado.equals(other.idtipopescado))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.backend.entidades.Tipopescado[ idtipopescado=" + idtipopescado + " ]";
    }
    
}
