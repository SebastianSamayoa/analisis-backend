package com.backend.repositorios;

import com.backend.entidades.Calidadproducto;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

/**
 *
 * @author jhoan
 */
public interface repocalidadproducto  extends PagingAndSortingRepository<Calidadproducto, Integer>, QueryByExampleExecutor<Calidadproducto> {
    
}
