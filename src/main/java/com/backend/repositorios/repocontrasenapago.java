package com.backend.repositorios;

import com.backend.entidades.Contrasenapago;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

/**
 *
 * @author jhoan
 */
public interface repocontrasenapago extends PagingAndSortingRepository<Contrasenapago, Integer>, QueryByExampleExecutor<Contrasenapago>{
    
}
