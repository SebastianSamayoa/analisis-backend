package com.backend.repositorios;

import com.backend.entidades.Controllote;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

/**
 *
 * @author jhoan
 */
public interface repocontrollote extends PagingAndSortingRepository<Controllote, Integer>, QueryByExampleExecutor<Controllote>{
    
}
