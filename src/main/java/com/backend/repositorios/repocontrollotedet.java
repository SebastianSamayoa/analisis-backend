package com.backend.repositorios;

import com.backend.entidades.Controllotedet;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

/**
 *
 * @author jhoan
 */
public interface repocontrollotedet extends PagingAndSortingRepository<Controllotedet, Integer>, QueryByExampleExecutor<Controllotedet> {
    
}
