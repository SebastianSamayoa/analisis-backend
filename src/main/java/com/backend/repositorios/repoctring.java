package com.backend.repositorios;

import com.backend.entidades.Ctringresomateriaprima;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

/**
 *
 * @author jhoan
 */
public interface repoctring extends PagingAndSortingRepository<Ctringresomateriaprima, Integer>, QueryByExampleExecutor<Ctringresomateriaprima>{
    
}
