package com.backend.repositorios;

import com.backend.entidades.Detalleingresomateriaprima;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

/**
 *
 * @author jhoan
 */
public interface repodetingre extends PagingAndSortingRepository<Detalleingresomateriaprima, Integer>, QueryByExampleExecutor<Detalleingresomateriaprima>{
    
}
