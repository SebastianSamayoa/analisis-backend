package com.backend.repositorios;

import com.backend.entidades.Estados;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

/**
 *
 * @author jhoan
 */
public interface repoestados extends PagingAndSortingRepository<Estados, Integer>, QueryByExampleExecutor<Estados>{
    
}
