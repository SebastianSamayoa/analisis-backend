package com.backend.repositorios;

import com.backend.entidades.Facturaloteproveedor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

/**
 *
 * @author jhoan
 */
public interface repofacturaloteproveedor extends PagingAndSortingRepository<Facturaloteproveedor, Integer>, QueryByExampleExecutor<Facturaloteproveedor>{
    
}
