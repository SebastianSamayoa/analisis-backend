package com.backend.repositorios;

import com.backend.entidades.Facturaproveedor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

/**
 *
 * @author jhoan
 */
public interface repofacturaproveedor extends PagingAndSortingRepository<Facturaproveedor, Integer>, QueryByExampleExecutor<Facturaproveedor>{
    
}
