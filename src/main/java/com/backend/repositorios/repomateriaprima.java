package com.backend.repositorios;

import com.backend.entidades.Materiaprima;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

/**
 *
 * @author jhoan
 */
public interface repomateriaprima extends PagingAndSortingRepository<Materiaprima, Integer>, QueryByExampleExecutor<Materiaprima>{
    
}
