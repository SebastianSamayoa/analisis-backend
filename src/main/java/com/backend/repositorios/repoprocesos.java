package com.backend.repositorios;

import com.backend.entidades.Procesos;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

/**
 *
 * @author jhoan
 */
public interface repoprocesos extends PagingAndSortingRepository<Procesos, Integer>, QueryByExampleExecutor<Procesos>{
    
}
