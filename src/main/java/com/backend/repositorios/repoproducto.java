package com.backend.repositorios;

import com.backend.entidades.Producto;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

/**
 *
 * @author jhoan
 */
public interface repoproducto extends PagingAndSortingRepository<Producto, Integer>, QueryByExampleExecutor<Producto>{
    
}
