package com.backend.repositorios;

import com.backend.entidades.Productosenviados;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

/**
 *
 * @author jhoan
 */
public interface repoproductosenviados extends PagingAndSortingRepository<Productosenviados, Integer>, QueryByExampleExecutor<Productosenviados>{
    
}
