package com.backend.repositorios;

import com.backend.entidades.Proveedores;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

/**
 *
 * @author jhoan
 */
public interface repoproveedores extends PagingAndSortingRepository<Proveedores, Integer>, QueryByExampleExecutor<Proveedores>{
    
}
