package com.backend.repositorios;

import com.backend.entidades.Tipoacceso;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

/**
 *
 * @author jhoan
 */
public interface repotipoacceso extends PagingAndSortingRepository<Tipoacceso, Integer>, QueryByExampleExecutor<Tipoacceso>{
    
}
