package com.backend.repositorios;

import com.backend.entidades.Tipopescado;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

/**
 *
 * @author jhoan
 */
public interface repotipopescado extends PagingAndSortingRepository<Tipopescado, Integer>, QueryByExampleExecutor<Tipopescado>{
    
}
