package com.backend.repositorios;

import com.backend.entidades.Usuarios;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

/**
 *
 * @author jhoan
 */
public interface repousuarios extends PagingAndSortingRepository<Usuarios, Integer>, QueryByExampleExecutor<Usuarios>{
    
}
